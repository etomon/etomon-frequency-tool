let {
    FrequencyTool
} = require('../lib');

let x = "18 18 * * 1,2,3,4,5";

let tool = FrequencyTool.fromCronString(x);
tool.removeDateTime(2);

console.log(tool.days);