import * as _ from 'lodash';
const moment = <any>require('moment-timezone');

export enum DAYS_OF_WEEK {
    Sunday = 0,
    Monday = 1,
    Tuesday = 2,
    Wednesday = 3,
    Thursday = 4,
    Friday = 5,
    Saturday = 6,
    '*' = Infinity
}


export enum RRULE_DAYS_OF_WEEK {
    SU = DAYS_OF_WEEK.Sunday,
    MO = DAYS_OF_WEEK.Monday,
    TU = DAYS_OF_WEEK.Tuesday,
    WE = DAYS_OF_WEEK.Wednesday,
    TH = DAYS_OF_WEEK.Thursday,
    FR = DAYS_OF_WEEK.Friday,
    SA = DAYS_OF_WEEK.Saturday,
}

export const DAYS_OF_WEEK_RRULE: string[] = [
   'SU',
   'MO',
   'TU',
   'WE',
   'TH',
    'FR',
    'SA'
];

export interface Hour {
    hour: number;
    minute: number;
}

/**
 * A small tool which allows you to generate iCal rrule strings and cron string from date information.
 *
 * MAS is short for "minutes after Sunday".
 * MAM is short for "minutes after midnight".
 */
export class FrequencyTool {
    constructor(public timezone: string = 'UTC', public dateTimes: Set<number> = new Set<number>()) {

    }

    /**
     * Returns a cloned FrequencyTool with the timezone altered to UTC
     */
    public utc(): FrequencyTool {
        return this.tz('UTC');
    }

    /**
     * Returns a cloned FrequencyTool with the timezone altered to the timezone provided
     *
     * @param timezone - Timezone to adjust for
     */
    public tz(timezone: string = 'UTC'): FrequencyTool {
        return new FrequencyTool(timezone, new Set<number>(Array.from(this.dateTimes.values())));
    }

    /**
     * Returns a moment instance prefilled with the timezone
     * @param zeroOut - If true, all clock attributes (hour/minute/second/millisecond) will be set to 0 (so midnight)
     * @protected
     */
    protected moment(zeroOut: boolean = false): any {
        let m = moment().tz(this.timezone);
        return zeroOut ? FrequencyTool.zeroOutMoment(m) : m;
    }

    /**
     * Sets the value of a list of days/times to the list of date times
     * @param day - Single day as number (0-6) use `Infinity` for all days.
     * @param MAM - MAM of the day in question.
     * @param value - Add or remove (check or uncheck) the days/times provided.
     */
    public setDateTime(day: DAYS_OF_WEEK, MAM: number, value: Boolean): void;
    /**
     *  Sets the value of  a list of days/times to the list of date times
     * @param days - An array of days as numbers (0-6).
     * @param MAM - An array of MAMs of the day in question
     * @param value - Add or remove (check or uncheck) the days/times provided.
     */
    public setDateTime(days: DAYS_OF_WEEK[], MAMs: number[], value: Boolean): void;
    /**
     *  Sets the value of  a list of days/times to the list of date times
     * @param days - An array of days as numbers (0-6).
     * @param MAM - An array of MAMs of the day in question
     * @param value - Add or remove (check or uncheck) the days/times provided.
     */
    public setDateTime(days: DAYS_OF_WEEK[], MAM: number, value: Boolean): void;
    /**
     *  Sets the value of  a list of days/times to the list of date times
     * @param day - Single day of days as number (0-6).
     * @param MAMs - An array of MAMs of the day in question
     * @param value - Add or remove (check or uncheck) the days/times provided.
     */
    public setDateTime(day: DAYS_OF_WEEK, MAMs: number[], value: Boolean): void;
    /**
     *  Sets the value of  a list of days/times to the list of date times
     * @param days - An array days or a single day as number (0-6). Use `Infinity` for all days.
     * @param MAMs - An array of MAMs or single MAM of the day in question.
     * @param value - Add or remove (check or uncheck) the days/times provided.
     */
    public setDateTime(days: DAYS_OF_WEEK|DAYS_OF_WEEK[], MAMs: number[]|number, value: Boolean): void {
        // Loop through all times
        MAMs = [].concat(MAMs).filter(b => typeof(b) === 'number' && !Number.isNaN(b));

        days = [].concat(days).filter(b => !Number.isNaN(b));
        // Infinity is short for all days of the week
        if (days.includes(Infinity)) {
            days = [0, 1, 2, 3, 4, 5, 6]
        }

        // If no numbers were passed in for times
        if (!MAMs.length) {
            // If this adding, copy all existing times
            if (value) MAMs = (MAMs.concat(this.times));
            else {
                MAMs = [];

                // Loop through the days provided
                for (let day of days) {
                    // Loop through all existing times,
                    // If one is between the start and end of the day in the loop
                    // add it to the MAM array
                    let start = (day * 1440), end = ((day + 1) * 1440);
                    for (let MAS of Array.from(this.dateTimes)) {
                        if (MAS >= start && MAS < end) {
                            // Subtract the time in seconds to get the MAM from MAS
                            MAMs.push(MAS-start);
                        }
                    }
                }
            }
        }

        else if (value || !days.length) days = (days.concat(this.days));
        MAMs = _.uniq(MAMs);
        days = _.uniq(days);

        for (let MAM of MAMs) {
            for (let day of days) {
                // Create a dummy moment object with the timezone set to the stored value
                let dummyDate = this.moment(true);
                dummyDate
                    .day(day)
                    .minute(MAM);

                this.dateTimes[value ? 'add' : 'delete'](FrequencyTool.MASForMoment(dummyDate));
            }
        }
    }

    /**
     * Converts an `Hour` object to MAM
     * @param hour - Hour object
     */
    public static hourToMAM(hour: Hour): number {
        return (
            (hour.hour * 60) +
            (hour.minute)
        )
    }

    /**
     * Converts MAM to an `Hour` object
     * @param hour - Hour object
     */
    public static MAMToHour(MAM: number): Hour {
        return (
            {
                hour: moment.duration({ minutes: MAM }).hours(),
                minute: moment.duration({ minutes: MAM }).minutes()
            }
        )
    }

    /**
     * Adds a list of days/times to the list of date times
     * @param day - Single day as number (0-6) use `Infinity` for all days.
     * @param MAM - MAM of the day in question
     */
    public addDateTime(day: DAYS_OF_WEEK, MAM: number): void;
    /**
     * Adds a list of days/times to the list of date times
     * @param days - An array of days as numbers (0-6).
     * @param MAM - An array of MAMs of the day in question
     */
    public addDateTime(days: DAYS_OF_WEEK[], MAMs: number[]): void;
    /**
     * Adds a list of days/times to the list of date times
     * @param days - An array of days as numbers (0-6).
     * @param MAM - An array of MAMs of the day in question
     */
    public addDateTime(days: DAYS_OF_WEEK[], MAM: number): void;
    /**
     * Adds a list of days/times to the list of date times
     * @param day - Single day of days as number (0-6).
     * @param MAMs - An array of MAMs of the day in question
     */
    public addDateTime(day: DAYS_OF_WEEK, MAMs: number[]): void;
    /**
     * Adds a list of days/times to the list of date times
     * @param days - An array days or a single day as number (0-6). Use `Infinity` for all days.
     * @param MAMs - An array of MAMs or single MAM of the day in question.
     */
    public addDateTime(days: DAYS_OF_WEEK|DAYS_OF_WEEK[], MAMs: number[]|number): void {
        return this.setDateTime(<any>days, <any>MAMs, true);

    }

    /**
     * Removes a list of days/times to the list of date times
     * @param day - Single day as number (0-6) use `Infinity` for all days.
     * @param MAM - MAM of the day in question
     */
    public removeDateTime(day: DAYS_OF_WEEK, MAM: number): void;
    /**
     * Removes a list of days/times to the list of date times
     * @param days - An array of days as numbers (0-6).
     * @param MAM - An array of MAMs of the day in question
     */
    public removeDateTime(days: DAYS_OF_WEEK[], MAMs: number[]): void;
    /**
     * Removes a list of days/times to the list of date times
     * @param days - An array of days as numbers (0-6).
     * @param MAM - An array of MAMs of the day in question
     */
    public removeDateTime(days: DAYS_OF_WEEK[], MAM: number): void;
    /**
     * Removes a list of days/times to the list of date times
     * @param day - Single day of days as number (0-6).
     * @param MAMs - An array of MAMs of the day in question
     */
    public removeDateTime(day: DAYS_OF_WEEK, MAMs: number[]): void;
    /**
     * Removes a list of days/times to the list of date times
     * @param days - An array days or a single day as number (0-6). Use `Infinity` for all days.
     * @param MAMs - An array of MAMs or single MAM of the day in question.
     */
    public removeDateTime(days: DAYS_OF_WEEK|DAYS_OF_WEEK[], MAMs: number[]|number): void {
        return this.setDateTime(<any>days, <any>MAMs, false);
    }

    /**
     * Sets all clock attributes (minute, hour, etc.) to `0` (midnight).
     * @param moment - Moment object to operate on.
     */
    public static zeroOutMoment(moment: any) {
        return moment.millisecond(0)
            .second(0)
            .hour(0)
            .minute(0)
            .day(0);
    }

    /**
     * Converts "minutes after Sunday" to a moment object.
     *
     * @param MAS - Minutes after sunday
     * @param timezone - A timezone to use, if none is provided will use UTC.
     */
    public static momentForMAS(MAS: number, timezone: string = 'UTC'): any {
        let dummyDate = moment().tz(timezone);

        return FrequencyTool.zeroOutMoment(dummyDate).minute(MAS);
    }

    /**
     * Converts a moment object to "minutes after Sunday".
     *
     * @param moment - Moment object
     */
    public static MASForMoment(moment: any): number {
        return (((moment.day() * 24) + moment.hours()) * 60) + moment.minutes();
    }

    /**
     * Returns an array of MAMs representing the selected repeating hours.
     * @example
     * let firstTime = freqTool.times[0];
     */
    public get times(): number[] {
        let results: Set<number> = new Set<number>();

        // Loop through all values
        for (let MAS of Array.from(this.dateTimes.values())) {
            // Create a dummy moment object with the timezone set to the stored value
            let dummyDate = FrequencyTool.momentForMAS(MAS, this.timezone);

            let MAM = (dummyDate.hours()*60)+dummyDate.minutes();
            results.add(MAM);
        }

        return Array.from(results.values());
    }

    /**
     * Returns an array of Hour objects representing the selected repeating hours.
     * @example
     * let { hour, minute } = freqTool.hours[0];
     * console.log(hour, minute);
     */
    public get hours(): Hour[] {
        return this.times.map(FrequencyTool.MAMToHour);
    }

    /**
     * Returns an array of numbers objects representing the selected repeating days;
     *
     * @example
     * console.log(freqTool.days); // --> [ 1, 5, 6 ]
     */
    public get days(): DAYS_OF_WEEK[] {
        let results: Set<DAYS_OF_WEEK> = new Set<DAYS_OF_WEEK>();

        // Loop through all values
        for (let MAS of Array.from(this.dateTimes.values())) {
             // Create a dummy moment object with the timezone set to the stored value
             let dummyDate = FrequencyTool.momentForMAS(MAS, this.timezone);

             results.add(<any>dummyDate.day());
        }

        return Array.from(results.values());
    }

    /**
     * Returns an array of date objects representing the selected repeating days within a given timeframe
     *
     * @example
     * console.log(freqTool.days); // --> [ 1, 5, 6 ]
     */
    public getDates(endDate: Date, startDate: Date = new Date()): Date[] {
        endDate = moment(endDate).tz(this.timezone).toDate();
        startDate = moment(startDate).tz(this.timezone).toDate();

        let results: Set<Date> = new Set<Date>();

        // Loop through all values
        let indexDate = new Date(startDate.getTime());
        while (indexDate.getTime() < endDate.getTime()) {
            for (let MAS of Array.from(this.dateTimes.values())) {
                // Create a dummy moment object with the timezone set to the stored value
                let dummyDate = FrequencyTool.zeroOutMoment(moment(indexDate)).minute(MAS);
                if (dummyDate.unix() < moment(startDate).unix() || dummyDate.unix() > moment(endDate).unix())
                    continue;

                results.add(<any>dummyDate.toDate());
            }
            indexDate = moment(indexDate).add('1', 'week').toDate();
        }

        return Array.from(results.values());
    }

    /**
     * Returns a single cron string for the date/times added to this object.
     */
    public toCronString(): string {
        let minute = this.hours.map(h => h.minute).join(',');
        let hour = this.hours.map(h => h.hour).join(',');
        let monthDay = '*';
        let month = '*';
        let weekDay = this.days.join(',');

        return [ minute, hour, monthDay, month, weekDay ].join(' ');
        // ? ? * * ?
    }

    /**
     * Returns a `FrequencyTool` object with data from the provided cron string.
     * @param cronString - Cron string to parse
     * @param timezone - Option timezone to set, defaults to UTC.
     */
    public static fromCronString(cronString: string, timezone?: string): FrequencyTool {
        let parts = cronString.split(" ");
        let result = new FrequencyTool(timezone);
        result.addDateTime(
            <DAYS_OF_WEEK[]>parts[4].split(',').map((p): DAYS_OF_WEEK => <DAYS_OF_WEEK>(p === '*' ? Infinity : Number(p))),
            parts[1].split(',').reduce((( arr, h ): number[] => {
                arr.push.apply(arr, parts[0].split(',').map(m => FrequencyTool.hourToMAM({
                    hour: Number(h.replace(/\*/ig, 'Infinity')),
                    minute: Number(m.replace(/\*/ig, 'Infinity'))
                })));

                return arr;
            }),(<number[]>[]))
        );
        return result;
    }

    /**
     * Returns a `FrequencyTool` object with data from the provided rrule string.
     * @param cronString - Cron string to parse
     * @param timezone - Option timezone to set, defaults to UTC.
     */
    public static fromRRuleString(rruleString: string, timezone?: string): FrequencyTool {
        let parts = rruleString
                .replace(/RRULE\:/ig, '')
                .split(';')
                .map(kvp => kvp.split('='));

        let wkDayStr: string = [].concat(parts.filter(([k, v]) => k === 'BYDAY')[0])[1] || '';
        let wkDays: DAYS_OF_WEEK[] = wkDayStr.split(',').map((d): DAYS_OF_WEEK => <DAYS_OF_WEEK>Number(RRULE_DAYS_OF_WEEK[<any>d]));
        let minuteStr: string = [].concat(parts.filter(([k,v]) => k === 'BYMINUTE')[0])[1] || '';
        let hourStr: string = [].concat(parts.filter(([k,v]) => k === 'BYHOUR')[0])[1] || '';
        let result = new FrequencyTool(timezone);
        result.addDateTime(
            wkDays,
            hourStr.split(',').reduce((( arr, h ): number[] => {
                arr.push.apply(arr, minuteStr.split(',').map(m => FrequencyTool.hourToMAM({
                    hour: Number(h),
                    minute: Number(m)
                })));

                return arr;
            }),(<number[]>[]))
        );
        return result;
    }

    /**
     * Returns a single rrule string for the date/times added to this object.
     */
    public toRRuleString(): string {
        let minute = this.hours.map(h => h.minute).join(',');
        let hour = this.hours.map(h => h.hour).join(',');
        let weekDay = this.days.map(n => DAYS_OF_WEEK_RRULE[n]).join(',');

        return `RRULE:BYDAY=${weekDay};BYHOUR=${hour};BYMINUTE=${minute};FREQ=WEEKLY`;
    }

    /**
     * Returns a single cron string for the date/times added to this object.
     */
    public toString(): string {
        return this.toCronString();
    }
}
