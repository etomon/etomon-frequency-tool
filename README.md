# Etomon Frequency Tool

![npm (scoped)](https://img.shields.io/npm/v/@etomon/etomon-frequency-tool)

## Usage
A small multipurpose lib for dealing with selecting time/date frequencies for repeating events.

The library is easy  to understand. See `sample/example.js` for a concrete example.

There is heavy documentation in the code itself `src/FrequencyTool`.

## Building

Etomon Frequency Tool is written in TypeScript.

To compile run `npm build`.
